package com.wsai.file;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * java.io.FileOutputStream
 * 用于向文件中写出字节的字节输出流。
 * 这是一个低级流。
 * 满足低级流特点：写出的数据目的地明确（文件中）
 *                真实将数据写入文件中。
 * @author WS
 */
public class FileOutputStreamDemo1 {
	public static void main(String[] args) throws IOException {
		/*
		 * FOS创建的第一种模式：覆盖写操作
		 * 默认创建出来的FOS就是覆盖写操作。意思是通过该流写出的文件会先将
		 * 文件中所有已有数据移除，重新写入新内容
		 *
		 * FOS创建的第二种模式：追加写操作
		 * 需要在构造方法中添加第二个参数：
		 * FileOutputStream(String path,boolean append)
		 * 第二个参数若为true，就是追加写模式，通过该流写出的数据会追加到当前文件末尾。
		 */
//		FileOutputStream fos = new FileOutputStream("F:\\GC\\output.txt",true); //true 追加写模式
		FileOutputStream fos = new FileOutputStream("F:\\GC\\output.txt"); //覆盖写操作
		String str = "我爱北京天安门！";
		byte[] data = str.getBytes("UTF-8");

		fos.write(data);

		System.out.println("写出完毕！");
		/*
		 * 流使用完毕后要关闭，来释放资源
		 */
		fos.close();
	}
}
